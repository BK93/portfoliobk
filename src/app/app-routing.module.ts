import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { ExperienceComponent } from './components/experience/experience.component';
import { SkillsComponent } from './components/skills/skills.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { EducationComponent } from './components/education/education.component';


const routes: Routes = [
  { path: '', component: HomeComponent, data: {state: 'home'}  },
  { path: 'about', component: AboutComponent, data: {state: 'about'}},
  { path: 'skills', component: SkillsComponent, data: {state: 'skills'}  },
  { path: 'education', component: EducationComponent, data: {state: 'education'}  },
  { path: 'experience', component: ExperienceComponent, data: {state: 'experience'}  },
  { path: 'portfolio', component: PortfolioComponent, data: {state: 'portfolio'}  },
  { path: 'contact', component: ContactComponent, data: {state: 'contact'}  },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
