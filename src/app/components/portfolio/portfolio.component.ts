import {Component, OnChanges} from '@angular/core';
import {PortfolioService} from '../../services/portfolio.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnChanges {

  items: any[];
  filterBy?: string = 'all';
  allItems: any[] = [];

  constructor(private portfolioService: PortfolioService ,private titleService: Title) {
    this.allItems = this.portfolioService.getItems();
    this.titleService.setTitle('Portfolio | Brian Kleijn');
  }
  ngOnChanges() {
    this.allItems = this.portfolioService.getItems();
  }
}
