import { Component, OnInit } from '@angular/core';
import Parallax from 'parallax-js';
import Typed from 'typed.js';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle('Welcome! | Brian Kleijn');
  }

  ngOnInit() {
    var scene = document.getElementById('scene');
    var parallaxInstance = new Parallax(scene);

    const options = {
      strings: ['Hello! My name is Brian.', 'Full Stack Developer.'],
      typeSpeed: 70,
      backSpeed: 70,
      showCursor: true,
      cursorChar: '|',
      loop: true
    };
    const typed = new Typed('.typed-element', options);
  }
}
