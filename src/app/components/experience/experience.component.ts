import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css',
    './timeline.scss']
})
export class ExperienceComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle('Experience | Brian Kleijn');
  }

  ngOnInit() {
  }

}
