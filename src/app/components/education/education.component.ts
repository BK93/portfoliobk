import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle('Education | Brian Kleijn');
  }

  ngOnInit() {
  }

}
