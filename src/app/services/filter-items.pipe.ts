import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterItems'
})
export class FilterItemsPipe implements PipeTransform {

  transform(items: any[], category: string): any {
    if(category === 'all'){ return items } else
      return items.filter(item =>{
        return item.category === category;
      });
  }

}
