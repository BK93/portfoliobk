import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {
  allItems = [];

  getItems() {
    return this.allItems = PortfolioDetails.slice(0);
  }

  getItemById(id: number) {
    return PortfolioDetails.slice(0).find(Images => Images.id === id);
  }
}

const PortfolioDetails = [
  { "id": 1, "category": "Laravel", "title": "Online Media Aggregator", "description": "Online Media Aggregator app created for the company RB-Media. Made with PHP/Laravel, Vuejs, Inertia, SQL.", "date": "2020/2021", "url": "../../../assets/portfolioImg/RB.png", "link": "" },
  { "id": 2, "category": "WordPress", "title": "Kleijn Coatings", "description": "Website created for the company Kleijn Coatings.", "date": "July 2021", "url": "../../../assets/portfolioImg/kleijncoatings.png", "link": "https://kleijncoatings.nl/" },
  { "id": 3, "category": "Angular", "title": "Personal website", "description": "Personal website to display my skills, education, experience and portfolio.", "date": "March 2020", "url": "../../../assets/portfolioImg/bkportfolio.png", "link": "" },
  { "id": 4, "category": "Angular", "title": "TrueLime Retrospective Tool", "description": "Scrum retrospective tool created for the company TrueLime. Made with Angular/.NET/NoSQL", "date": "January 2020", "url": "../../../assets/portfolioImg/retrospectiveapp.png", "link": "" },
  { "id": 5, "category": "NoSQL", "title": "Studdit API", "description": "School project, API made in NodeJS and NoSQL.", "date": "December 2019", "url": "../../../assets/portfolioImg/mongodb.jpg", "link": "" },
  { "id": 6, "category": "Vue", "title": "Nostradamus Clocking application", "description": "Clocking application created for the company Nostradamus. Made with Vuejs/NodeJS/SQL.", "date": "July 2019", "url": "../../../assets/portfolioImg/nostradamus.png", "link": "" },
  { "id": 7, "category": "WordPress", "title": "DOB Buildings", "description": "Website created for the company DOB Buildings.", "date": "May 2019", "url": "../../../assets/portfolioImg/dobportfolio.png", "link": "https://dobbuildings.nl/" },
  { "id": 8, "category": "HTML/CSS", "title": "Beauty Salon", "description": "Website created for a beauty salon in Breda.", "date": "November 2017", "url": "../../../assets/portfolioImg/salon.png", "link": "http://schoonheidssalonwillekevangool.nl/" },
  { "id": 9, "category": "Various", "title": "Various school projects", "description": "Various school projects created with various programming languages", "date": "2018/2022", "url": "../../../assets/portfolioImg/avans.jpg", "link": "" },
];
