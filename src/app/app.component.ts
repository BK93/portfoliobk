import { Component } from '@angular/core';
import {transition, trigger, useAnimation} from '@angular/animations';
import {rotateRoomToBottom, rotateRoomToLeft, rotateRoomToRight, rotateRoomToTop} from 'ngx-router-animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations:  [
    trigger('home',  [ transition('home => *', useAnimation(rotateRoomToLeft))]),
    trigger('about',  [ transition('about => *', useAnimation(rotateRoomToBottom))]),
    trigger('skills',  [ transition('skills => *', useAnimation(rotateRoomToTop))]),
    trigger('education',  [ transition('education => *', useAnimation(rotateRoomToRight))]),
    trigger('experience',  [ transition('experience => *', useAnimation(rotateRoomToLeft))]),
    trigger('portfolio',  [ transition('portfolio => *', useAnimation(rotateRoomToBottom))]),
    trigger('contact',  [ transition('contact => *', useAnimation(rotateRoomToTop))])
  ]

})
export class AppComponent {
  getState(outlet)  {
    return outlet.activatedRouteData.state;
  }
}
